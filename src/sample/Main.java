package sample;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.util.ArrayList;

public class Main extends Application {

    ArrayList<String[]>andmebaas = new ArrayList<>();
    static VBox tabel = new VBox();
    static int kogusumma = 0;
    static  Label summaLabel = new Label();

    @Override
    public void start(Stage primaryStage) throws Exception {
        VBox vbox = new VBox(); // layout, paigutus, väljanägemine
//        vbox.setSpacing(20);
        Scene scene = new Scene(vbox, 400, 600);
        primaryStage.setScene(scene); // stage on aken
        primaryStage.show();

        // ÜLEMINE SEKTSIOON
        summaLabel.setText("0€");
        StackPane summaPane = new StackPane();
        summaLabel.setPadding(new Insets(64)); // nähtamatu ala teksti ümber
        summaLabel.setFont(new Font(32));
        summaPane.getChildren().add(summaLabel);

        // KESKMINE SEKTSIOON
        HBox kuluSisestamineHBox = new HBox();
        TextField kuluPealkiri = new TextField(); // saab teksti trükkida
        kuluPealkiri.setPromptText("Kulutuse liik");
        TextField kuluSumma = new TextField();
        kuluSumma.setPromptText("Kulu summa");
        Button liitmine = new Button("+");
        Button lahutamine = new Button("-");
        kuluSisestamineHBox.getChildren().addAll(kuluPealkiri, kuluSumma, liitmine, lahutamine);

        // ALUMINE SEKTSIOON
        vbox.getChildren().addAll(summaPane, kuluSisestamineHBox, tabel);

        // ANDMETE SISESTAMINE
        lahutamine.setOnMouseClicked(mouseEvent -> {
            lisaAndmebaasi(kuluPealkiri.getText(), kuluSumma.getText(), true);
            kuluPealkiri.clear();
            kuluSumma.clear();
        });

        liitmine.setOnMouseClicked(mouseEvent -> {
            lisaAndmebaasi(kuluPealkiri.getText(), kuluSumma.getText(), false);
            kuluPealkiri.clear();
            kuluSumma.clear();
        });


    }
    public static void  lisaAndmebaasi(String tyyp, String summa, boolean lahuta){ // meetod
        kogusumma -= Integer.parseInt(summa);
        kogusumma += Integer.parseInt(summa);
        Label rida = new Label(tyyp + " " + summa + "€");
        tabel.getChildren().add(rida);
        summaLabel.setText(kogusumma + "€");
    }

    public static void main(String[] args) {
        launch(args);
    }
}
